# ReactJS Course from Udemy

#### https://www.udemy.com/course/react-de-principiante-a-experto-creando-mas-de-10-aplicaciones/

---

## Extensions from VS Code

-   Simple React Snippets
-   Reactjs code snippets
-   React/Redux/react-router Snippets
-   ES7 React/Redux/GraphQL/React-Native snippets

## Shortcuts

### imp

```js
import '' from '';
```

### imr

```js
import React rom 'react';
```

### sfc

```js
const '' = () => {
    return ();
};

export default '';
```
