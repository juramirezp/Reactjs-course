import React from "react";

const Product = ({ product, cart, setCart, products }) => {
    const { id, nombre, precio } = product;

    // Agregar producto al carrito
    const selectProduct = (id) => {
        const product = products.filter((p) => p.id == id)[0];
        setCart([...cart, product]);
    }

    // Eliminar productod el carro
    const deletProduct = (id) => {
        const products = cart.filter(product => product.id !== id)
        setCart(products);
    }

    return (
        <div>
            <h2>{nombre}</h2>
            <p>{precio}</p>
            
            {products
            ?(
                <button type="button" onClick={() => selectProduct(id)}>
                    Comprar
                </button>
            )
            :(
                <button type="button" onClick={() => deletProduct(id)}>
                    Eliminar
                </button>
            )
            }
        </div>
    );
}

export default Product;
