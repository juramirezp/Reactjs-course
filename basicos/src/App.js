import React, { Fragment, useState } from "react";
import Header from "./components/Header";
import Footer from "./components/Footer";
import Product from "./components/Product";
import Cart from "./components/Cart";

function App() {
    // Crear Listado de productos
    const [products, setProducts] = useState([
        {
            id: 1,
            nombre: "Camisa Negra",
            precio: 19,
        },
        {
            id: 2,
            nombre: "Camisa Roja",
            precio: 86,
        },
        {
            id: 3,
            nombre: "Camisa Verde",
            precio: 35,
        },
        {
            id: 4,
            nombre: "Camisa Blanca",
            precio: 23,
        },
    ]);

    // State para carrito de compra
    const [cart, setCart] = useState([]);

    // Obtener fecha
    const fecha = new Date().getFullYear();

    return (
        <Fragment>
            <Header title="Header Tienda Virtual" />
            <h1>Lista de Productos</h1>
            {products.map((product) => (
                <Product
                    key={product.id}
                    product={product}
                    cart={cart}
                    setCart={setCart}
                    products={products}
                />
            ))}

            <Cart cart={cart} setCart={setCart}/>

            <Footer fecha={fecha} />
        </Fragment>
    );
}

export default App;
